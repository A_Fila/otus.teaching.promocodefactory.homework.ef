﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : ControllerBase
    {
        IRepository<Preference> _preferenceRepository;

        public PreferenceController(IEnumerable<IRepository<Preference>> repositories)
        {
            _preferenceRepository = repositories.OfType<EfRepository<Preference>>().SingleOrDefault(); //(r => r is EfRepository<Customer>).FirstOrDefault();

            if (_preferenceRepository == null)
                throw new Exception("Service EfRepository<Preference> hasn't been injected to container");
        }

        /// <summary>
        /// Получение списка предпочтений клиентов
        /// </summary>
        /// <returns>список предпочтений в формате JSON.</returns>
        [HttpGet]
        public async Task<List<PreferenceResponse>> GetPrererencesAsync()
        {
            var prefs = await _preferenceRepository.GetAllAsync();
            var prefsModelList = prefs.Select(p => new PreferenceResponse()
            {
                Id = p.Id,
                Name = p.Name
            }).ToList();

            return prefsModelList;
        }


        //public IActionResult Index()
        //{
        //    return View();
        //}
    }
}
