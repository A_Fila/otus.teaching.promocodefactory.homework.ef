﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NuGet.Packaging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        IRepository<Customer> _customerRepository;
        IRepository<Preference> _preferenceRepository;
        IRepository<CustomerPreference> _cpRepository;

        public CustomersController(IEnumerable<IRepository<Customer>> custRepos, IRepository<Preference> prefRepo, IRepository<CustomerPreference> cpRepo) 
        {
            _customerRepository = custRepos.OfType<EfRepository<Customer>>().SingleOrDefault(); //(r => r is EfRepository<Customer>).FirstOrDefault();

            if (_customerRepository == null) 
                throw new Exception("Service EfRepository<Customer> hasn't been injected to container");

            _preferenceRepository = prefRepo;
            _cpRepository = cpRepo;

        }
        /// <summary>
        /// Получение списка клиентов
        /// </summary>
        /// <returns>список клиентов в формате JSON.</returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()//ActionResult
        {
            //TODO: Добавить получение списка клиентов
            //throw new NotImplementedException();
            var customers = await _customerRepository.GetAllAsync();
            var customersModelList = customers.Select(c => new CustomerShortResponse()
            {
                Id = c.Id,
                Email = c.Email,
                FirstName = c.FirstName,
                LastName = c.LastName
            }).ToList();

            return customersModelList;
        }
        /// <summary>
        /// [R] Получение клиента вместе с выданными ему промомкодами
        /// </summary>
        /// <param name="id">GUID клиента</param>
        /// <returns></returns>
        [HttpGet("{id}")]      //[ActionName(nameof(GetCustomersAsync))]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
             //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            //throw new NotImplementedException();
            var customer = await _customerRepository.GetByIdAsync(c => c.Id == id, ["Preferences", "PromoCodes"]);

            if (customer == null)
                return NotFound();

            var customerModel = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = customer.Preferences.Select(p => p.Name).ToList(),
                PromoCodes = customer.PromoCodes.Select(p => new PromoCodeShortResponse 
                {
                    Id=p.Id,    
                    Code = p.Code,
                    ServiceInfo = p.ServiceInfo,
                    BeginDate  = p.BeginDate.ToUniversalTime().ToString(),
                    EndDate =   p.EndDate.ToUniversalTime().ToString(),
                    PartnerName = p.PartnerName
                }).ToList()
            };

            return customerModel;

        }

        /// <summary>
        /// [C] Cоздание нового клиента со списком предпочтений
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            //throw new NotImplementedException();

            //create customer
            var newCust = new Customer()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email                
            };
            
            //add customer to repository
            await _customerRepository.AddAsync(newCust);           
            await _customerRepository.SaveChangesAsync(); // save changes to db

            //get preferences
            List<Preference> prefs = (await _preferenceRepository.GetAllAsync())
                        .Where(p => request.PreferenceIds.Contains(p.Id)).ToList();


            //fill CustomerPreference list
            var custPrefList = prefs.Select(p => new CustomerPreference { CustomerId = newCust.Id, PreferenceId = p.Id });

            //add prefs to CustomerPreference repo
            foreach (var cp in custPrefList)
                await _cpRepository.AddAsync(cp);

            await _cpRepository.SaveChangesAsync(); //save changes to db //_customerRepository

            newCust.Preferences = prefs; //for output to response

            var res = CreatedAtAction(nameof(GetCustomerAsync),
                                      new { newCust.Id },
                                      newCust);
            //var res = CreatedAtAction("GetCustomer", 
            //                          "Customers", 
            //                          new { newCust.Id }, 
            //                          newCust); //work!!!

            return res; //Ok();// 
        }
        /// <summary>
        /// [U] Редактирование данных клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            //throw new NotImplementedException();
            var cust = await _customerRepository.GetByIdAsync(c => c.Id == id, ["Preferences"]); 

            if (cust == null) 
                return NotFound();

            //edit
            cust.FirstName = request.FirstName;
            cust.LastName = request.LastName; 
            cust.Email = request.Email;
            cust.Preferences.Clear();                     //clear all old prefs

            await _customerRepository.SaveChangesAsync(); //save

            //get preferences from db
            List<Preference> prefs = (await _preferenceRepository.GetAllAsync())
                        .Where(p => request.PreferenceIds.Contains(p.Id)).ToList();

            //fill CustomerPreference list
            var custPrefList = prefs.Select(p => new CustomerPreference { CustomerId = cust.Id, PreferenceId = p.Id });

            //add prefs to CustomerPreference repo
            foreach (var cp in custPrefList)
                await _cpRepository.AddAsync(cp);

            await _cpRepository.SaveChangesAsync(); //save changes to db //_customerRepository

            return Ok();//!!!!!!!!!!!!!!!!!!
        }

        /// <summary>
        /// Удаление клиента вместе с выданными ему промокодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            //throw new NotImplementedException();

            var isDeleted = await _customerRepository.DeleteByIdAsync(id);
            if (isDeleted) _customerRepository.SaveChanges();

            return (isDeleted)? Ok()
                              : NotFound();
        }
    }
}