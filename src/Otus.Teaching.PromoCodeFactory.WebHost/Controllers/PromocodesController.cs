﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        EfRepository<PromoCode> _promoRepository;

        public PromocodesController(IRepository<PromoCode> promoRepo)     
        {
            _promoRepository = promoRepo as EfRepository<PromoCode> ;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            //TODO: Получить все промокоды 
            //throw new NotImplementedException();
            var promos = await _promoRepository.GetAllAsync();
            var promosModelList = promos.Select(p => new PromoCodeShortResponse
            {
                Id = p.Id,
                Code = p.Code,
                ServiceInfo = p.ServiceInfo,

                BeginDate = p.BeginDate.ToLocalTime().ToString(),
                EndDate = p.EndDate.ToLocalTime().ToString(),
                
                PartnerName = p.PartnerName                
            }).ToList();

            return promosModelList;
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            //throw new NotImplementedException();

            if (request == null) //|| true
                return BadRequest(new Exception("A non-empty request body is required."));

            //find pref in db
            Preference pref = _promoRepository.FindAllBy<Preference>(p => p.Name == request.Preference) //1) request.Preference.ToLower(); -doesn't work correctly in SQL 
                                              .FirstOrDefault();                                        //2) String.Compare(p.Name, request.Preference, StringComparison.OrdinalIgnoreCase) == 0 - error

            if (pref == null)
            {
                var allPref = _promoRepository.FindAllBy<Preference>( p => true).Select(p => p.Name).ToArray();
                var exMsg = $"Preference <{request?.Preference}> not found.\r\n" +
                            $"Pass one from list: [{string.Join(", ", allPref)}].";

                return BadRequest(new Exception(exMsg));
            }

            //get customer
            var cust = _promoRepository.FindAllBy<Customer>(c => c.Preferences.Any(p=> p.Name == request.Preference))
                                       .FirstOrDefault();
                        
            if (cust == null)
                return BadRequest(new Exception($"No customer with preference <{pref?.Name}>"));


            var emp = _promoRepository.FindAllBy<Employee>(e => e.Role.Name == "Admin").FirstOrDefault();


            //new promo
            var newPromo = new PromoCode()
            {
                Code = request.PromoCode,
                PartnerName = request.PartnerName,
                ServiceInfo = request.ServiceInfo,
                Preference = pref,
                BeginDate = DateTime.Now,
                CustomerId = cust.Id,
                PartnerManager = emp                
            };

            await _promoRepository.AddAsync(newPromo);
            await _promoRepository.SaveChangesAsync(); 

            return Ok();

        }
    }
}