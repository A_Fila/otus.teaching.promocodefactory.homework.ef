﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
//using Otus.Teaching.PromoCodeFactory.WebHost;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataBaseContext: DbContext
    {
        public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options)   {}

        //DbSets
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<Customer> PromoCodes { get; set; }

        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        protected override void ConfigureConventions(ModelConfigurationBuilder configurationBuilder)
        {
            base.ConfigureConventions(configurationBuilder);

            configurationBuilder.Properties<string>()
                .HaveMaxLength(255);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); //???

            //CustomerPreference
            var custPrefList = FakeDataFactory.Customers
                .SelectMany(c => c.Preferences,
                 (c, p) => new { CustomerId = c.Id, PreferenceId = p.Id });        //(c, p) => new { CustomersId = c.Id, PreferencesId = p.Id });

            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Preferences)
                .WithMany(p => p.Customers)
                .UsingEntity<CustomerPreference>(j => j.HasData(custPrefList));


            modelBuilder.Entity<CustomerPreference>().Ignore(c => c.Id);

            //modelBuilder.Entity<Customer>()
            //    .HasMany(c => c.Preferences)
            //    .WithMany(p => p.Customers)
            //    .UsingEntity(j => j.HasData(custPrefList));


            //.UsingEntity<CustomerPreference>(
            //    j => j.HasOne(cp => cp.Preference).WithMany(p => p.CustomerPreferences),
            //    j => j.HasOne(cp => cp.Customer).WithMany(p => p.CustomerPreferences),
            //    j => j.HasData(custPrefList)
            // );


            //.UsingEntity(j => j.HasData(custPrefList));

            //modelBuilder.Entity<Customer>().HasMany(c => c.Preferences)
            //                               .WithMany(p => p.Customers)
            //                               .UsingEntity<Dictionary<string, object>>("CustomerPreference",
            //                                              r => r.HasOne<Preference>().WithMany().HasForeignKey("PreferencesId"),
            //                                              l => l.HasOne<Customer>().WithMany().HasForeignKey("CustomersId"),
            //                                              j =>
            //                                              {
            //                                                  j.HasKey("CustomersId", "PreferencesId");
            //                                                  j.HasData(cpList);
            //                                              }
            //                                            );


            //Preferences
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);

            //customers
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers.Select(c => new
            {
                c.Id,
                c.Email,
                c.FirstName,
                c.LastName
            }));

            //Roles
            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);

            //Employee
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees.Select(e => new
            {
                e.Id,
                e.FirstName,
                e.LastName,
                e.Email,
                RoleId = e.Role.Id,
                e.AppliedPromocodesCount
            }));

            //PromoCodes
            modelBuilder.Entity<PromoCode>().HasData(FakeDataFactory.PromoCodes.Select(p => new
            { 
                p.Id,
                p.Code,
                p.ServiceInfo,
                p.BeginDate, 
                p.EndDate,
                p.PartnerName,
                PartnerManagerId = p.PartnerManager.Id,
                PreferenceId = p.Preference.Id,
                p.CustomerId
            }));

            //modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            //var erList = FakeDataFactory.Employees
            //    .SelectMany(e => e.Role,
            //                (e, r) => new { CustomersId = c.Id, PreferencesId = p.Id });
            //modelBuilder.Entity<Employee>().OwnsOne(p => p.Role).HasData(

            //    );
            //

            //modelBuilder.Entity<Employee>()
            //    .OwnsOne(e => e.Role)
            //    .HasData(FakeDataFactory.Employees);

            //modelBuilder.Entity<Employee>().Property(c => c.Id).HasMaxLength(1);
            //modelBuilder.SetDefaultMaxStringLength(255);
        }

 

    }  
}
//************************************** ADD MIGRATIONS***********************************
//from Package Manager Console run:
//  0) cd Otus.Teaching.PromoCodeFactory.DataAccess
//  1) dotnet ef migrations add Initial --startup-project ../Otus.Teaching.PromoCodeFactory.WebHost/
//  2) dotnet ef database update --startup-project ../Otus.Teaching.PromoCodeFactory.WebHost/
//  3) dotnet ef migrations remove --startup-project ../Otus.Teaching.PromoCodeFactory.WebHost/
//****************************************************************************************

//targetAssembly = the target project you are operating on. On the command line, it is the project in the current working directory. In Package Manager Console, it is whatever project is selected in the drop down box on the top right of that window pane.
//migrationsAssembly = assembly containing code for migrations. This is configurable. By default, this will be the assembly containing the DbContext, in your case, Project.Data.dll. As the error message suggests, you have have a two options to resolve this

//1 - Change target assembly.

//cd Project.Data/
//dotnet ef --startup-project ../Project.Api/ migrations add Initial

//// code doesn't use .MigrationsAssembly...just rely on the default
//options.UseSqlServer(connection)
//2 - Change the migrations assembly.

//cd Project.Api/
//dotnet ef migrations add Initial

//// change the default migrations assembly
//options.UseSqlServer(connection, b => b.MigrationsAssembly("Project.Api"))