﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий для работы с Entity Framework
    /// </summary>
    /// <typeparam name="T">Тип сущности</typeparam>
    public class EfRepository<T> : IRepository<T> where T: BaseEntity
    {
        protected readonly DbContext Context;
        private readonly DbSet<T> _entitySet;

        public EfRepository(DataBaseContext context)
        {
            Context = context;
            _entitySet = Context.Set<T>();
        }

        public async Task<T> AddAsync(T element)
        {
            //throw new NotImplementedException();
            return (await _entitySet.AddAsync(element)).Entity;
        }

        public async Task<bool> DeleteByIdAsync(Guid id)
        {
            //throw new NotImplementedException();
            var entity = await _entitySet.FirstOrDefaultAsync(e => e.Id == id);
            if (entity == null) return false;

            _entitySet.Remove(entity);

            return true;
        }

        public Task<IEnumerable<T>> GetAllAsync()//Task<IEnumerable<T>>
        {
            return Task.FromResult<IEnumerable<T>>(_entitySet);
        }
        public Task<IQueryable<T>> GetAllAsync(string[] navProps)//Task<IEnumerable<T>>
        {
            IQueryable<T> query = _entitySet;

            foreach (var entity in navProps)
                query = query.Include(entity);

            return Task.FromResult(query);
        }

        public async Task<T> GetByIdAsync(Guid id)
        {   
            //throw new NotImplementedException();            

            return await _entitySet.FindAsync(id);
        }

        public async Task<T> GetByIdAsync(Expression<Func<T, bool>> filter, string[] navProps)
        {          
            IQueryable<T> query = _entitySet;
            
            foreach (var entity in navProps)
                query = query.Include(entity);

            return await query.Where(filter).FirstOrDefaultAsync();
        }

        public virtual IQueryable<T> FindAllBy<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            if (predicate == null)
                throw new ArgumentNullException("Predicate value must be passed to FindAllBy<T>.");

            
           return Context.Set<T>().Where(predicate);
        }
        #region SaveChanges

        /// <summary>
        /// Сохранить изменения.
        /// </summary>
        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        /// <summary>
        /// Сохранить изменения асинхронно
        /// </summary>
        public async Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            await Context.SaveChangesAsync(cancellationToken);
        }

        #endregion

    }
}