﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task<T> AddAsync(T element);

        Task<bool> DeleteByIdAsync(Guid id);

        Task<T> GetByIdAsync(Expression<Func<T, bool>> filter, string[] navProps);

        Task SaveChangesAsync(CancellationToken cancellationToken = default);
        void SaveChanges();
    }
}