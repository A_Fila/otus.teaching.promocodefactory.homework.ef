﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public class BaseEntity
    {
        [StringLength(36, ErrorMessage = "{0} length must be equal {1}.", MinimumLength = 36)]
        public Guid Id { get; set; }
    }
}